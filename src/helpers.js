import {computed, ref} from "vue";
import {ethers} from "ethers";

export const helpers = () => {
    const account = ref(null);
    const chainId = ref(null);
    const isConnected = computed(() => Boolean(account.value));
    const { ethereum } = window
    const currentProvider = new ethers.providers.Web3Provider(ethereum, 'any')

    const connect = async () => {
        try {
            await ethereum?.send('eth_requestAccounts', [])
        } catch (e) {
            console.error(e)
        }
    }

    const requestSwitchEthChain = async (
        chainId,
    ) => {
        await currentProvider.send('wallet_switchEthereumChain', [
            { chainId: `0x${chainId}` },
        ])
        await init()
    }

    const updateProviderState = async () => {
        try {
            const network = await currentProvider.detectNetwork()
            chainId.value = network.chainId
            const currentAccounts = await currentProvider.listAccounts()
            if (currentAccounts.length > 0) {
                account.value = currentAccounts[0]
            }
        } catch (e) {
            console.error(e)
        }
    }

    const _setListeners = () => {
        const tempProviderStub = currentProvider.provider

        tempProviderStub.on('accountsChanged', () => {
            updateProviderState()
        })
        tempProviderStub.on('chainChanged', () => {
            updateProviderState()
        })
        tempProviderStub.on('disconnect', () => {
            account.value = null
        })
    }

    const init = async () => {
        _setListeners()
        await updateProviderState()
    }

    const signAndSendTransaction = async (txRequestBody) => {
        try {
            const signer = currentProvider.getSigner()
            const gasPrice = await currentProvider.getGasPrice()

            const rawTx = {
                from: txRequestBody.from,
                to: txRequestBody.to,
                gasPrice,
                gasLimit: 300000,
                data: txRequestBody.data,
                value: txRequestBody.value,
                chainId: txRequestBody.chain_id,

            }
            console.log(rawTx.chainId)
            const transactionResponse = await signer.sendTransaction(rawTx)
            await transactionResponse.wait()
            return transactionResponse
        } catch (e) {
            console.error(e)
        }
    }

    return {
        ethereum,
        chainId,
        connect,
        signAndSendTransaction,
        init,
        isConnected,
        requestSwitchEthChain,
    }
}
